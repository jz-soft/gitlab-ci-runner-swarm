#!/bin/bash

echo "Check Runner config..."
cat /etc/gitlab-runner/config.toml
echo ""

if [[ -z "$(cat /etc/gitlab-runner/config.toml | grep -o token)" ]]; then
    echo -e "registering runner...\n"
    /usr/bin/gitlab-runner register --non-interactive \
    --run-untagged=true --locked=false \
    --executor docker \
    --docker-image "docker:latest" \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock"

    echo -e "Check Runner config...\n"
    cat /etc/gitlab-runner/config.toml
fi

/usr/bin/gitlab-runner run --user=gitlab-runner --working-directory=/home/gitlab-runner
